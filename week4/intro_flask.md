# Personal VM

For the rest of the course you will be working on your own VM. For today we have created a VM for you, but later on
we will show you how to create your own VM.

  * Each VM has a public IP address which you can SSH to from the class VM (coe332.tacc.utexas.edu).
  * Find the IP address for your VM in the markdown file on the class bitbucket repo here: (https://bitbucket.org/tacc-cic/coe332/src/master/week4/xsede_account_and_vms.md)
  * First SSH to coe332.tacc.utexas.edu as you normally would.
  * Then from there, SSH to the IP address using the `ubuntu` user; for example, I would use: `ssh ubuntu@129.114.16.83`

A few comments on the VM:

  * As the `ubuntu` user, you have `sudo` access on your VM. You can switch the root user with the command `$ sudo su - ` and get back to the ubuntu user using `exit`
  * Be careful! As root you can literally do anything, including removing your entire file system. If that happens, you could lose work and in the short term, we will have to create another VM for you which could take a few days.

Make sure you can SSH to your personal VM.


# Introduction to the Flask Web Server Microframework

Flask is a Python library and framework for building web servers. Some of the defining characteristics of flask make it
a good fit for this project:

  * Flask is small, relatively easy to use and get setup initially.
  * Flask is "robust" - a great fit for REST APIs and "microservices".
  * When used correctly, Flask is performant enough to handle the traffic of sites with 100Ks users.

## Setup and Installation

The flask library is not part of the Python standard library but can be installed standard tools like `pip`. We already
did this for your current VM but here are the instructions which you could need later when using another VM:

  1. SSH to the VM.
  2. Install pip for python3 if it is not already installed (`apt-get update && apt-get install -y python3-pip`).
  2. Execute `pip install flask`

## A Hello World Flask App

Create a file called app.py and open it for editing.

  1. Import the Flask class: `from flask import Flask`
     At the heart of every flask-based web program is a "Flask application" object. The application object holds the
     primary configuration and behaviors of the web program.
  2. Create a Flask application object passing `__name__` to the constructor`: `app = Flask(__name__)`
     Note that `__name__` is a special python variable that gets set to either the module's actual name OR "__main__"
     in the case where the module was executed directly by the python interpreter.
  3. Launch the development server using the `app.run` method if the app.py module is executed from the command line:

```
from flask import Flask

app = Flask(__name__)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
```

Notes:

  * The `debug=True` tell flask to print verbose debug statements while the server is running.
  * The `host=0.0.0.0` instructs the server to listen on all network interfaces; basically this means you can reach
    the server from insiade and outside the host VM.

## Run the Flask App and Make an Initial Request
To start our Flask app simply run it like you would any other python program:

```
  $ python3 app.py
 * Serving Flask app "app" (lazy loading)
 * Environment: production
   WARNING: Do not use the development server in a production environment.
   Use a production WSGI server instead.
 * Debug mode: on
 * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 250-539-093

```

That's it! We now have a server up and running. Some notes:

  * Note that the program took over our shell; we could have put it in the background, but for now we want to leave it in the foreground. (Multiple PIDs are started for the flask app when started in daemon mode; to get them, find all processes listening on the port 5000 socket with `lsof -i:5000`).
  * If we make changes to our flask app while the server is running, the server will detect those changes automatically and "reload"; you will see a log to the effect of `Detected change in <file>`.
  * We can stop the program with `Ctrl+C` just like any other (python) program.
  * If we stop our flask program, the server will no longer be listening and our requests will fail.

Let's try to talk to it. Note this line:
```
 * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
```
That tells us our server is listening on all interfaces on the default Flask port, port 5000.

### A Word on Ports

Ports are a concept from networking that allow multiple services or programs to be running at the same time, listening for messages over the internet, on the same computer.

 * For us, ports will always be associated with a specific IP address. In general, we specify a port by combining it with an IP separated by a colon (:) character. For example, `129.114.97.16:5000`.
 * One and only one program can be listening on a given port at a time.
 * Some ports are designated for specific activities; Port 80 is reserved for HTTP, port 443 for HTTPS (encrypted HTTP) but other ports can be used for HTTP/HTTPS traffic.

In a separate terminal, SSH to your VM again.

We'll use the command line HTTP client `curl` to make a request to our Flask app on port 5000;

### curl Basics:

You can think of `curl` as a command-line version of a web browser: it is just an HTTP client.

  * The basic syntax is `curl <some_url>:<some_port>`. This will make a GET request to the URL and port print the message response.
  * Curl will default to using port 80 for http and port 443 for https.
  * You can specify the HTTP verb to use with the `-X` flag; e.g., `curl -X GET <some_url>` (though `-X GET` is redundant because curl defaults to making a GET request.

  * You can set "verbose mode" with the `-v` flag, which will then show additional information such as the headers passed back and forth (more on this later).

To make a request , type the following:

```
  $ curl localhost:5000
```

You should see:

```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>
```

Our server is sending us HTML! It's sending a 404 that it could not find the resource we requested. It's time to add some routes.


## Routes in Flask

In a Flask app, you define the URLs in your application using the `app.route` decorator.

  * `app.route` is a decorator - place it on the line before the declaration of a python function.
  * `app.route` requires a string argument which is the path of the URL (not including the base_url);
  * `app.route` takes an argument `methods` which should be a list of strings containing the names of valid HTTP methods.
  * When the URL + HTTP method combination is requested, Flask will call the decorated function.


## (Warning - tangent!) What is a Python decorator?

  * A decorator is a function that takes another function as an input and returns a different function then extends the behavior in some way.
  * The decorator must return a function which includes a call to the original function plus the extended behavior.
  * The typical structure of a decorator is as follows:


```
def my_decorator(some_func):
    def func_to_return():
        # extend the behavior of some_fun by doing some processing before it is called (optional)
        do_something_before()
        # call the original function
        some_func(*args, **kwargs)
        # extend the behavior of some_fun by doing some processing after it is called (optional)
        do_something_after()
    return func_to_return
```

As an example, consider this test program:

```
def print_dec(f):
    def func_to_return(*args, **kwargs):
        print("args: {}; kwargs: {}".format(args, kwargs))
        val = f(*args, **kwargs)
        print("return: {}".format(val))
        return val
    return func_to_return

@print_dec
def foo(a):
    return a+1


result = foo(2)
print("Got the result: {}".format(result))
```

Our print decorator gets executed automatically when we call `foo(2)`.

### Defining the Hello World Route
Let's define a hello world route for the base url. To do so, add the following code:

```
@app.route('/', methods=['GET'])
def hello_world():
    return "Hello world"
```

Back in the other SSH terminal, execute the curl command again; you should see:

```
  $ curl localhost:5000
Hello world
```

### Routes with URL Parameters

Flask makes it easy to create Routes (or URLs) with variable in the URL. Here are the basics:

 * We put the variable name in angled brackets (`<>`) within the app.route() decorator statement; for example `@app.route(/<year>`) for a variable `year`.
 * We make the variable a parameter to the decorated function and use it just like any other variable.

In the following example, we create a route with a variable:

```
@app.route('/<name>', methods=['GET'])
def hello_name(name):
    return "Hello {}\n".format(name)
```

## Combing Flask with HW 1

We are now ready to use flask to make a RESTful API that uses the functions we wrote in homework 1. We need to think through the following:

  * What are the nouns in our application?
  * What are the routes we want to define?
  * How will users supply parameters to our API endpoints?
  * What data format do we want to return?

