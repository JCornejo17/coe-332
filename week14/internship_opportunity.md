# Internship Opportunities

## CIC Internship

We will have an opening for an internship in our group (Cloud and Interactive Computing - CIC). Dates and salary are
still TBD but will be similar to the Jetstream REU above.

The work will focus on building features and performance optimizations for our Abaco (Actor-based-containers) computing
 platform: https://abaco.readthedocs.io

There will be an opportunity to write up the work and present at a conference.

If you are interested please email me directly, jstubbs@tacc.utexas.edu


## Jetstream REU 

You can find some info here: https://jetstream-cloud.org/research/reu.php 

Planned dates 5/30/19 - 8/01/19, stipend (approximately $4000) + housing expenses and travel to/from IU included

The theme will be machine learning in the cloud. 

If you have questions, please email reu@jetstream-cloud.org.

