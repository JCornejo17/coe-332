# Homework 1

### Preliminary Steps

You will either need a local Python 3.7 and git working environment, or you can set one up easily on the class VM.

- SSH to `coe332.tacc.utexas.edu`. Within your home director, you should:
    - Create a Python 3 Virtual Environment with the command `python3 -m venv ~/coe332-venv`
    - Activate the environment with the command `source ~/coe332-venv/bin/activate`
    - Install `coverage` in your personal environment with `pip install coverage`

### The assignment

The homework assignment instructions and files are available at [https://bitbucket.org/jchuahtacc/coe332-f2019-hw1](https://bitbucket.org/jchuahtacc/coe332-f2019-hw1)

Briefly, you will be:

- Writing a few Python functions to read and manipulate data
- Using git to provide a work history for your homework
- Using Bitbucket to submit and verify your homework